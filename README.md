# Procedura di implementazione di un nuovo backup RMAN su ZFS per Postevita

## Filesystem ZFS

### Creazione

1.  Eseguire il login come `root` sulla console ZFS corretta 
(Ad Es [`https://192.168.168.218:215`](https://192.168.168.218:215 "Console ZFS Prod. testa 2"))

2.  Cliccare su `PROJECTS` e successivamente sul tasto `+`

3.  Inserire il nome del database lasciado invariati gli altri campi e cliccare su `APPLY`

4.  Cliccare sull'icona a forma di matita a destra del progetto appena creato per entrare nella scermata di edit.

5.  Cliccare sulla scheda `General` e assicurarsi di impostare i parametri come in figura:

    ![Creazione-ZFS-Progetto-Generale](/img/zfs_prj_general.png)

    Dopodiché cliccare su `APPLY` per salvare le impostazioni

6.  Cliccare sulla scheda `Shares` e successivamente sul tasto `+`

7.  inserire "backup" nel campo `Name` come in figura:

    ![Creazione-ZFS-Share-Create](/img/zfs_share_create.png)

    Cliccare su `APPLY` per salvare le impostazioni.

### Procedura di Mount

Eseguire i seguenti comandi da utente root per ogni nodo del rac (sostituire C2PSPREP con il progetto corretto)

```bash
PRJ_NAME=C2PSPREP

mkdir /zfssa/PROD/${PRJ_NAME}/backup

# eseguire il seguente comando oppure aggiungere la riga a mano sostituendo le variabili
cat >> /etc/fstab << EOF
rmpf-zfsib:/export/${PRJ_NAME}/backup      /zfssa/PROD/${PRJ_NAME}/backup      nfs     rw,bg,hard,nointr,rsize=32768,wsize=32768,tcp,actimeo=0,vers=3,timeo=600
EOF

mount /zfssa/PROD/${PRJ_NAME}/backup
```

## Aggiunta Database sul catalogo RMAN
